# App del corso React AriesTech

[![NPM Version][npm-image]][npm-url]
[![NPM Downloads][downloads-image]][downloads-url]
[![Node.js Version][node-version-image]][node-version-url]
[![Build Status][travis-image]][travis-url]
[![Test Coverage][coveralls-image]][coveralls-url]

Progetto creato per il corso Aries

## Installazione

Per installare questo pacchetto bisogna andare su [NPM](https://nodejs.org) ed eseguire il codice:

```sh
$ npm install my-app
```