import React from 'react';
import './App.css';
// import Header from './components/Header';
// import SectionCryptoPayment from './components/SectionCryptoPayments';
// import SectionCounter from './components/SectionCounter';
// import { Main } from  './shared/components/'
import { NavbarCustom } from './shared/components';
import { AppRoutes } from './shared/routes'
import { BrowserRouter as Router } from 'react-router-dom'
import { CarouselRoadMap } from '../../../../day1/react/my-app/src/test/component'
import { TimeLine } from './test/component/Timeline';

function App() {
  return (
    <>
      <Router>
        <div className='App'>

          <NavbarCustom/>

          <AppRoutes></AppRoutes>

        </div>
      </Router>
      <CarouselRoadMap/>
      <TimeLine/>





    </>
  );
}


export default { App };

