import './MioComponente.css';

import { Button, StyledButton, Link, StyledLink, Input } from './shared/components'

// import { Row, Col } from 'react-bootstrap';

// type mioCompomnenteParamps = {
//     label: string,
//     cssClass?: string
// }


// const MioComponente = (par: mioCompomnenteParamps) => {
const MioComponente = () => {

    // const { label, cssClass } = par;

    return (
        // <Row>
        //     <Col>
        //         <h1 className={cssClass ? cssClass : "defaultClass"}>{label}</h1>
        //     </Col>
        // </Row>
        <div>

            <StyledButton primary color={'#ffffff'}>
                <span>Styled Button</span>
            </StyledButton>

            <StyledButton>
                <span>Styled Button 2</span>
            </StyledButton>
            
            <Button primary color={'#ffffff'}> 
                <span>Button</span>
            </Button>

            <br />
            <Link primary>pippoLink</Link>

            <br />

            <StyledLink >styledLink Pippo</StyledLink>

            <br />

            <Input/>




        </div>
        
    );
}

export default MioComponente;