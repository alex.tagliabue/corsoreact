import { API_URL_ARTICLE } from "../../shared/const";
import { ArticleType } from "../types";
import axios from 'axios';

export const callCreateArticle = async (a: ArticleType): Promise<ArticleType> => {

    console.log('async start');

    const callResultArticle = await axios.post(`${API_URL_ARTICLE}`, a)
        .then((response) => {
            console.log('anadata a fuon fine la post');
                return response.data;    
        })
        .catch((e) => {
            throw e;
        })

    return callResultArticle;

}