import { Input } from "..";
import { Button } from '../../../shared/components';
import { CardArticle } from "..";
import { useState } from 'react'

import { useStoreArticle } from '../../store';
import { useArticleFunctions } from "../.."; 

export const Article = () => {

    const { storeArticle } = useStoreArticle();
    const { startSagaArticle } = useArticleFunctions();
       
    const [title, setTitle] = useState("");
    const [body, setBody] = useState("");

    const randomId = () =>{
        return Math.floor(Math.random() * 10);
    }
    
    const article = {
        title: title,
        body: body,
        userId: randomId(),
    }

    return (
        <>
            <div>
                <div>
                    <label>Title</label>
                    <Input value={title} textChanged={(val: string) => { setTitle(val) }} />
                </div>

                <div>
                    <label>Body</label>
                    <Input value={body} textChanged={(val: string) => { setBody(val) }} />
                </div>

                <div>
                    <Button click={(output?: boolean) =>{startSagaArticle(article)}}>invia articolo</Button>
                </div>
            </div>

            <CardArticle article={storeArticle}/>
        </>
    );
} 