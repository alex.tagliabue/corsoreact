import { ArticleType, ArticleTypeProps } from '../../types'
import { Card } from 'react-bootstrap'

export const CardArticle = (a: ArticleTypeProps) => {
    const { title, body, userId } = a.article;
    return (
        
        <Card style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">{userId}</Card.Subtitle>
                <Card.Subtitle className="mb-2 text-muted">{body}</Card.Subtitle>
            </Card.Body>
        </Card>
    );
}
