import { useDispatch } from 'react-redux';
import { useCallback } from 'react';

import { startSagaSendArticleAction} from '../../store/actions'
import { ArticleType } from '../../types';

export const useArticleFunctions = () => {

    const dispatch = useDispatch();

    const startSagaArticle = useCallback((r:ArticleType)=>{
        dispatch(startSagaSendArticleAction(r))
    },[dispatch])

    return { startSagaArticle }
}