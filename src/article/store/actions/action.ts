import { createAction } from "typesafe-actions";
import { ArticleType } from "../..";

const startSagaSendArticle = "article/startSagaSendArticle";
const callLoadArticle = "article/callLoad";
const articleSent = "article/sent";

export const startSagaSendArticleAction = createAction(startSagaSendArticle)<ArticleType>();

export const callLoadArticleAction = createAction(callLoadArticle)();

export const articleSentAction = createAction(articleSent)<ArticleType>();