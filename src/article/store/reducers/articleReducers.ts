import * as actions from '../actions';
import { articleState } from '../../types';

import { ActionType, createReducer } from 'typesafe-actions'

const initialState: articleState = {
    loading: false,
    article: {title:'', body:''},
}

export type articleActionList = ActionType<typeof actions>

export type callLoadArticleActionType = ActionType<typeof actions.callLoadArticleAction>

export type articleSentActionType = ActionType<typeof actions.articleSentAction>

const callLoadArticleActionTypeCase = (state: articleState, action: callLoadArticleActionType): articleState => ({
    ...state, loading: true

});

const carticleSentActionTypeCase = (state: articleState, action: articleSentActionType): articleState => ({
    ...state, article: {...action.payload}
});

export const articleReducer = createReducer<articleState, articleActionList>(initialState).
    handleAction(
        actions.callLoadArticleAction,
        callLoadArticleActionTypeCase
    ).handleAction(
        actions.articleSentAction,
        carticleSentActionTypeCase
    )