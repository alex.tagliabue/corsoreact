import * as actions from '../actions'
import { call, put, takeLatest } from 'redux-saga/effects'
import { callCreateArticle } from '../../api'
import { ArticleType } from '../..'

function* loadArticleSaga(action: ReturnType<typeof actions.startSagaSendArticleAction>) {

    yield put(actions.callLoadArticleAction())

    
    try {
        let articleResponse: ArticleType = yield call(callCreateArticle as any, action.payload)
        yield put(actions.articleSentAction(articleResponse));

    } catch (error) {

    }
}

export function* articleSaga() {
    yield takeLatest(actions.startSagaSendArticleAction, loadArticleSaga)
}