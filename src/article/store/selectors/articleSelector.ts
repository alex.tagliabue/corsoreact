import { createSelector } from 'reselect';
import { useSelector } from 'react-redux';
import { articleState } from '../..';

const sliceState = (state: { articleState: articleState }) => state.articleState;

const storeArticleSelector = createSelector(sliceState, slice => slice.article)

const loadingSelector = createSelector(sliceState, slice => slice.loading)

export const useStoreArticle = () => {
    const storeArticle= useSelector(storeArticleSelector);
    return { storeArticle }
}

export const useArticleAreLoading = () => {
    const loading = useSelector(loadingSelector);
    return { loading }
}