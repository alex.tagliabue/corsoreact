import { ArticleType } from "./articleType";

export type ArticleTypeProps = {
    article: ArticleType
}