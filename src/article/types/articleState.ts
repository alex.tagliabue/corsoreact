import { ArticleType } from ".";

export type articleState = {
    loading:boolean;
    article:ArticleType;
}