export type ArticleType ={
    title: string,
    body: string,
    userId?: number,
} 