import { Button } from 'react-bootstrap';

type myBottonParamps = {
    label: string,
    cssClass: string
}

const ButtonCustom = (par:myBottonParamps) => {

    return (
        <>
            <Button className={par.cssClass}>{par.label}</Button>{' '}
        </>
    );
}

export default ButtonCustom;