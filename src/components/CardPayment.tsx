import { Col } from 'react-bootstrap'
import './CardPayment.css'

type miaCardParams = {
    title: string,
    content: string
}

const CardPayment = (par: miaCardParams) => {
    return (
        <Col xs={6} className='card-payment text-center '>

            <div className='container-img'>
                <img src="https://maxsilvercoaching.com/masko/images/crypto-payment/icon/icon-1.png" alt="" />
            </div>
            <h5>{par.title}</h5>
            <p>{par.content}</p>


        </Col>
    );
}

export default CardPayment;