import Navbar from './Navbar'
import SectionHero from './SectionHero';


const Header = () => {
    return (
        <header>
            <Navbar/>
            <SectionHero/>
        </header>
    );
}

export default Header;