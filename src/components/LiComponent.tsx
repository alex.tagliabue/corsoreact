import './LiComponent.css'

type mioNavParams = {
    linkName: string,
    LinkRef?: string
}

const LiComponent = (par: mioNavParams) => {

    const { linkName, LinkRef } = {...par};


    return (
        <li>
            <a href={LinkRef}>{linkName}</a>
        </li>
    );
}

export default LiComponent;