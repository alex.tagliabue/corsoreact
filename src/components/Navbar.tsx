import { Nav } from 'react-bootstrap';
import ButtonCustom from './ButtonCustom';

import './Navbar.css';

import LiComponent from './LiComponent';
import { useMemo } from 'react';


const Navbar = () => {
    
    type mioMenuItem = {
        linkName: string,
        LinkRef: string
    }

    // const vociMenu = ['Home', 'Pages','Blog','Contact'];
    const vociMenu:mioMenuItem[]=[];

    let home:mioMenuItem={
        linkName: 'home',
        LinkRef: "./home"
    }

    let pages:mioMenuItem={
        linkName: 'Pages',
        LinkRef: "./Pages"
    }

    let blog:mioMenuItem={
        linkName: 'Blog',
        LinkRef: "./Blog"
    }

    let contact:mioMenuItem={
        linkName: 'Contact',
        LinkRef: "./Contact"
    }

    vociMenu.push(home);
    vociMenu.push(pages);
    vociMenu.push(blog);
    vociMenu.push(contact);

    const menu = vociMenu.map((voce)=>{
        console.log('Voce menu ' + voce);
        
        return <LiComponent linkName={voce.linkName} LinkRef={voce.LinkRef}/>
    });

    const Menueffect = useMemo(()=>{
        console.log('menu effect');
        
        return menu;
    },[menu])

    return (
        <Nav>
            <div>
                <img src="https://maxsilvercoaching.com/masko/images/logo.png" alt="Logo" />
            </div>

            <ul>
                {Menueffect}
            </ul>

            <ButtonCustom label={'Trial version'} cssClass={'white'}/>
        </Nav>
    )
}

export default Navbar;