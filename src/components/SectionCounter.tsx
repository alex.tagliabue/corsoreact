import {Row} from 'react-bootstrap';
import SingleCounter from './SingleCounter';
import './SectionCounter.css';
import { useMemo } from 'react';

const SectionCounter = () =>{

    type myData = {
        counter: number,
        label: string,
    }

    const myCounterData:myData[] = [];

    let happyClinets:myData={
        counter:864,
        label:'Happy Clients'
    }

    let linesOfCode:myData={
        counter:536,
        label:'Lines of Code'
    }

    let cupOfCofee:myData={
        counter:795,
        label:'Cup of Cofee'
    }

    let totalDownload:myData={
        counter:967,
        label:'Total download'
    }

    myCounterData.push(happyClinets);
    myCounterData.push(linesOfCode);
    myCounterData.push(cupOfCofee);
    myCounterData.push(totalDownload);

    const data = myCounterData.map((data)=>{
        return  <SingleCounter counter={data.counter} label={data.label}/>
    })

    const dataEffect = useMemo(()=>{
        return data;
    },[data])

    return(
        <Row className='counter-inner'>
            {/* <SingleCounter counter={864}label={'Happy Clients'}/> */}
            {dataEffect}
        </Row>
    );
}

export default SectionCounter;