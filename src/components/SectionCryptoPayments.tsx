import { Row, Col, Image } from 'react-bootstrap';
import CardPayment from './CardPayment';


const SectionCryptoPayment = () => {

    type miaCardDetails = {
        title: string,
        content: string
    }

    const vociCard:miaCardDetails[]=[];

    let card1:miaCardDetails={
        title: 'Quick Payment',
        content: "Proident eske cupa offia excepteur them ecat cupidat deserunt mollit anim"
    }

    let card2:miaCardDetails={
        title: 'Crypto Payments',
        content: "Proident eske cupa offia excepteur them ecat cupidat deserunt mollit anim"
    }

    vociCard.push(card1);
    vociCard.push(card2);

    const card = vociCard.map((voce)=>{
        return <CardPayment title={voce.title} content={voce.content}/>
    })

    return (
        <Row>
            <Col xs={6}>
                <Image src="https://maxsilvercoaching.com/masko/images/crypto-payment/shape-left.png" alt="" fluid/>
            </Col>

            <Col xs={6}>
                <Row>
                    {card}
                </Row>
            </Col>
        </Row>
    );
}

export default SectionCryptoPayment;