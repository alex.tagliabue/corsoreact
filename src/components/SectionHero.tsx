import { Row, Col } from 'react-bootstrap';
import ButtonCustom from './ButtonCustom';
import './SectionHero.css';

const SectionHero = () => {
    return (
        <section className='section-hero'>
            <Row>
                <Col xs={6}>
                    <h1>Spend, pay & store on your terms.</h1>
                    <p>Accept Crypto payments from your customers on your smartphone.</p>
                    <ButtonCustom label={'Get started now'} cssClass={'blue'} />

                </Col>

                <Col xs={6}>
                    <img src="https://maxsilvercoaching.com/masko/images/hero-right.png" alt="" />

                </Col>

            </Row>
        </section>
    );
}

export default SectionHero;