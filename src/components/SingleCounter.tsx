import { Col } from "react-bootstrap";
import './SingleCounter.css';

type myCounterProp = {
    counter: number,
    label: string,
}

const SingleCounter = (prop:myCounterProp) => {
    return(
        <Col xs={3}>
            <div className="single-counter text-center">
                <span className="counter">{prop.counter}</span>
                <p>{prop.label}</p>

            </div>
        
        </Col>
    );
}

export default SingleCounter;