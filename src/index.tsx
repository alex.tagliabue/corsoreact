import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import APPobj from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import { createStore, applyMiddleware, compose } from "redux"; //redux
import { Provider } from "react-redux"; //redux
import { rootReducer } from './shared/store'
import createSagaMiddleware from 'redux-saga'
import { rootSaga } from './shared/store';  //saga

var win = window as any;

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = win.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const { App } = APPobj; 

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(sagaMiddleware))); //redux
sagaMiddleware.run(rootSaga); //saga

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>

      <div>
        <App />
      </div>

    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
