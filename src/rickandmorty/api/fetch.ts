import { API_URL_RICK } from "../../shared/const";
import { CharacterListDetail } from "../types";
import axios from 'axios';


export const callCharacterList = async (page: number): Promise<CharacterListDetail> => {
    console.log('async start');

    const callResult = await axios.get(`${API_URL_RICK}/character?page=${page}`)
        .then((r) => {
            if (r.status === 200) {
                console.log(r.data);
                return r.data;
            }
            else {
                return null;
            }
        })
        .catch((e) => {
            throw e;
        })

    return callResult;
}
