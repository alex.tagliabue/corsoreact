import { Card } from "react-bootstrap";
import { CharacterDetail } from "../../types";

export const CharacterCard = (c: { character: CharacterDetail }) => {
    return (
        <Card style={{ width: '18rem' }} className="mb-5">
            <Card.Body>
                <Card.Title>{c.character.name}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">{c.character.gender}</Card.Subtitle>
                <Card.Subtitle className="mb-2 text-muted">{c.character.species}</Card.Subtitle>
                <Card.Subtitle className="mb-2 text-muted">{c.character.status}</Card.Subtitle>                
                <Card.Img variant="top" src={c.character.image} ></Card.Img>
            </Card.Body>
        </Card>
    );
}