import { Row } from 'react-bootstrap';
import { useEffect } from 'react';
import { callCharacterList } from '../../api'
import { CharacterCard } from '../CharacterCard'
import { useMemo } from 'react';
import { useStoreCharacter } from '../../store';
import { useCharacterFunctions } from '../../hooks';

export const RickAndMorty = () => {

    const { startSagaCharacter } = useCharacterFunctions();
    const currentPage:number = 1;

    useEffect(() => {
        if (currentPage) {
            startSagaCharacter({ page: currentPage });
        }
    }, [currentPage]);

    useEffect(()=>{
        callCharacterList(1)       
        return ;
    },[])

    const {storeCharacter} = useStoreCharacter() 

    const showListaCharacter = useMemo(() => {
        return storeCharacter.map((c) => <CharacterCard key={c.id} character={c} />)
    }, [storeCharacter]);


    return(
        <>
        <Row className='justify-content-around'>
            {showListaCharacter}
        </Row>
        </>
    );
}

