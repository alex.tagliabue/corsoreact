import { useDispatch } from 'react-redux';
import { useCallback } from 'react';

import { startSagaLoadCharacterAction} from '../../store/actions'
import { startSagaPayload } from '../../types';

export const useCharacterFunctions = () => {

    const dispatch = useDispatch();

    const startSagaCharacter = useCallback((r:startSagaPayload)=>{
        dispatch(startSagaLoadCharacterAction(r))
    },[dispatch])

    return { startSagaCharacter }
}