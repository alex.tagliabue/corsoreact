import { createAction } from "typesafe-actions";
import { CharacterListDetail, CharacterDetail, startSagaPayload } from "../..";

const startSagaLoadCharacter = "rickandmorty/startSalgaLoad";
const callLoadCharacter = "rickandmorty/callLoad";
const characterLoaded = "rickandmorty/loaded";


export const startSagaLoadCharacterAction = createAction(startSagaLoadCharacter)<startSagaPayload>();

export const callLoadCharacterAction = createAction(callLoadCharacter)();

export const characterLoadedAction = createAction(characterLoaded)<CharacterDetail[]>();