import * as actions from '../actions';
import { characterState } from '../../types';

import { ActionType, createReducer } from 'typesafe-actions'

const initialState: characterState = {
    loading: false,
    character: [],
}

export type characterActionList = ActionType<typeof actions>

export type callLoadCharacterActionType = ActionType<typeof actions.callLoadCharacterAction>

export type characterLoadedActionctionType = ActionType<typeof actions.characterLoadedAction>


const callLoadCharacterActionTypeCase = (state: characterState, action: callLoadCharacterActionType): characterState => ({
    ...state, loading: true
});

const characterLoadedActionctionTypeCase = (state: characterState, action: characterLoadedActionctionType): characterState => ({
    ...state, character: [...action.payload], loading: false
});

export const characterReducer = createReducer<characterState, characterActionList>(initialState).
    handleAction(
        actions.callLoadCharacterAction,
        callLoadCharacterActionTypeCase
    ).handleAction(
        actions.characterLoadedAction,
        characterLoadedActionctionTypeCase
    )