import * as actions from '../actions'
import { call, put, takeLatest } from 'redux-saga/effects'
import { callCharacterList } from '../../api'
import { CharacterListDetail } from '../..'

function* loadCharacterSaga(action: ReturnType<typeof actions.startSagaLoadCharacterAction>) {

    yield put(actions.callLoadCharacterAction())
    try {
        let characterResponse: CharacterListDetail = yield call(callCharacterList, action.payload.page)
        yield put(actions.characterLoadedAction(characterResponse.results));

    } catch (error) {

    }
}

export function* characterSaga() {
    yield takeLatest(actions.startSagaLoadCharacterAction, loadCharacterSaga)
}