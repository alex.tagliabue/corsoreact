import { createSelector } from 'reselect';
import { useSelector } from 'react-redux';
import { characterState } from '../..';

const sliceState = (state: { characterState: characterState }) => state.characterState;

const storeCharacterSelector = createSelector(sliceState, slice => slice.character)

const loadingSelector = createSelector(sliceState, slice => slice.loading)

export const useStoreCharacter = () => {
    const storeCharacter= useSelector(storeCharacterSelector);
    return { storeCharacter }
}

export const useCharacterAreLoading = () => {
    const loading = useSelector(loadingSelector);
    return { loading }
}