export type Info = {
    count: number;
    pages: number;
    next: string;
    prev?: any;
}

export type Origin = {
    name: string;
    url: string;
}

export type Location = {
    name: string;
    url: string;
}

export type CharacterDetail = {
    id: number;
    name: string;
    status: string;
    species: string;
    type: string;
    gender: string;
    origin: Origin;
    location: Location;
    image: string;
    episode: string[];
    url: string;
    created: Date;
}

export type CharacterListDetail = {
    info: Info;
    results: CharacterDetail[];
}

export type startSagaPayload = {
    page:number
}


