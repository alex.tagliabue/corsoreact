import { CharacterDetail } from ".";

export type characterState = {
    loading:boolean;
    character:CharacterDetail[];
}