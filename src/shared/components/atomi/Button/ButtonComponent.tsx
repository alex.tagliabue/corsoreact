import styled from 'styled-components';
import React, { ReactElement, FC } from 'react';

export interface buttonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    primary?: boolean;
    click?: (param?: boolean) => void;
    //void ci dice che è una funzione
}

const StyledButton = styled.button<buttonProps>`
    background-color: ${p => p.primary ? 'blue' : 'gray'};
    color: ${p => p.primary ? 'white' : (p.color ? p.color : '#fff533')}
`;

const Button: FC<buttonProps> = ({ primary, click, ...props }): ReactElement => {

    const hadleClick = () => {
        if (click) {
            click()
        };
    }

    return (
        <StyledButton primary={primary} {...props} onClick={() => { hadleClick() }}>
            {props.children}
        </StyledButton>
    );
}

export { Button, StyledButton };