import styled from 'styled-components';
import React, { ReactElement, FC } from 'react';

export interface checkboxProps extends React.InputHTMLAttributes<HTMLInputElement> {
    checkChange?: (checked: boolean) => void
}

const StyledCheckBox = styled.input <checkboxProps>`

`;

const CheckBox: FC<checkboxProps> = ({ checkChange, ...props }): ReactElement => {

    const hadleCheckClick = (checked: boolean) => {
        if (checkChange) {
            checkChange(checked)
        };
    }

    return (
        <StyledCheckBox type="checkbox" {...props} onChange={(e) => { hadleCheckClick(e.target.checked) }}>
            {props.children}
        </StyledCheckBox>
    );
}

export { CheckBox, StyledCheckBox };