import styled from 'styled-components';
import React, { ReactElement, FC } from 'react';

export interface linkProps extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
    primary?: boolean
}

const StyledLink = styled.a<linkProps>`
    color: '#7fffD4';
    &:hover {
        color: ${p => p.primary ? '#000000' : '#ff0000'};
    }
`;

const Link: FC<linkProps> = ({ primary, ...props }): ReactElement => {
    return (
        <StyledLink primary={primary} {...props}>
            {props.children}
        </StyledLink>
    );
}

export { Link, StyledLink };