import styled from 'styled-components';

const StyledH1 = styled.h1`
    padding: 22px 0;
    font-size: 33px;
`;


const H1 = () =>{
    return(
        <StyledH1></StyledH1>
    );

}

export {H1, StyledH1};

