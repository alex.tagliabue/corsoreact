import { Spinner } from 'react-bootstrap';
import { useMemo } from 'react';
import { useUserAreLoading } from '../../../../user/store'
import { useCoinsAreLoading } from '../../../../web3/store/'
import { useCharacterAreLoading } from '../../../../rickandmorty/store/'
const Loader = () => {

    const { loading } = useUserAreLoading();
    const { loading : coinsLoading } = useCoinsAreLoading();
    const { loading : characterLoading } = useCharacterAreLoading();

    const show = useMemo(() => {

        return (loading || coinsLoading || characterLoading)?
            <div style={{ display: 'block', position: 'absolute', width: '100%', minHeight: '100vh', backgroundColor: '#00000030' }}>
                <Spinner animation="border" role="status">
                    <span className="visually-hidden">Loading...</span>
                </Spinner>
            </div> : null
    }, [loading,coinsLoading,characterLoading])

    return (
        <>
            {show}
        </>
    );
}

export { Loader }