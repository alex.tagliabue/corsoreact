import { StyledH1 } from '../../atomi'

const Main = () => {
    return (
        <div>
            <StyledH1>Titolo Styled H1</StyledH1>
        </div>
    );
}

export { Main };