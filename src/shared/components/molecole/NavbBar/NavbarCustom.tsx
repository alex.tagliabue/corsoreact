import { Navbar, Container, Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const NavbarCustom = () => {
    return (
        <>
            <Navbar bg="dark" expand="lg" variant="dark" sticky="top" className="mb-5">
                <Container>
                    <Navbar.Brand href="#home">Corso React</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link ><Link to="/">Home</Link></Nav.Link>
                            <Nav.Link ><Link to="/users">Users</Link></Nav.Link>
                            <Nav.Link ><Link to="/web3">Web 3</Link></Nav.Link>
                            <Nav.Link ><Link to="/rickandmorty">Rick and Morty</Link></Nav.Link>
                            <Nav.Link ><Link to="/article">Article</Link></Nav.Link>

                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    );
}

export { NavbarCustom }