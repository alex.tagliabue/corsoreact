import { useState, useMemo, useEffect } from 'react';
import { Button, Input, CheckBox } from '../../atomi';
import { callCreateUser, User, UserCard, callUsersList, ListUserCard, userListResponse, userList } from '../../../../user';
import { Row } from 'react-bootstrap';
import { useUserFunctions, useStoreUsers } from '../../../../user';

const SearchBar = () => {

    const [name, setName] = useState("");
    const [job, setJob] = useState("");
    const [createdUser, setCreatedUser] = useState<User>();

    const [filteredUsers, setFilteredUsers] = useState<userList[]>([]);
    const [searchTxt, setSearchTxt] = useState("");

    const [pages, setPages] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);

    const { storeusers } = useStoreUsers();
    const { startSagaLoaduser } = useUserFunctions();

    const searchClick = async (output?: boolean) => {
        let u: User = {
            name: name,
            job: job,
        }

        let cu = await callCreateUser(u);
        console.log(cu);
        console.log(typeof cu);

        if (cu && cu.id) {
            setCreatedUser(cu);
            setName("");
            setJob("");
        }

    };


    useEffect(() => {
        setCurrentPage(0);
    }, [])


    useEffect(() => {

        let filtered = storeusers.filter((u) => {
            if (u.first_name.toLowerCase().includes(searchTxt.toLowerCase()) || u.last_name.toLowerCase().includes(searchTxt.toLowerCase())) {
                return u;
            }
        })

        setFilteredUsers(filtered);

    }, [searchTxt, storeusers]);


    useEffect(() => {
        if (currentPage) {
            startSagaLoaduser({ page: currentPage });
        }
    }, [currentPage]);


    const ListUserCards = useMemo(() => {
        return filteredUsers.map((u) => <ListUserCard key={u.id} user={u} />)
    }, [filteredUsers]);


    const PageButtons = useMemo(() => {
        let buttons = [];

        for (let index = 1; index <= pages; index++) {
            buttons.push(<Button primary={index === currentPage} key={index} click={() => { setCurrentPage(index) }}>{index}</Button>)
        }
        return buttons;

    }, [pages, currentPage]);


    const inputCheckChanged = (checked: boolean) => {
        console.log(`CheckBox: ${checked}`);
    }

    return (
        <div>
<Input/>


            {createdUser?.id}
            <br />


            {createdUser ?
                <div>
                    <UserCard user={createdUser} />
                    <Button click={(output?: boolean) => { setCreatedUser(undefined) }}>Reset Card</Button>

                </div> :
                <div>In attesa di creazione</div>
            }
            <br />


            <CheckBox checkChange={(checked: boolean) => { inputCheckChanged(checked) }}></CheckBox>
            <br />



            <Input value={searchTxt} textChanged={(val: string) => { setSearchTxt(val) }} />
            <br />
            <Button click={(output?: boolean) => { setCurrentPage(1) }}>Lista Utenti</Button>
            <br />


            <div>
                {PageButtons}
            </div>
            <Row className="justify-content-center">
                {ListUserCards}
            </Row>


        </div>
    );
}

export { SearchBar };