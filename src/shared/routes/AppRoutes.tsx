import { Col, Container, Row } from "react-bootstrap";
import { Loader, SearchBar } from "../components";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import { Web3 } from "../../web3";
import { RickAndMorty } from "../../rickandmorty/components/RickAndMorty";
import { Article } from "../../article/components";

export const AppRoutes = () => {

    return (
        <Routes>
            <Route path="/" element={
                <>
                    <div>Home page</div>
                </>

            }>
            </Route>

            <Route path="/users" element={
                <>
                    <Loader />
                    <Container fluid className='hero-area'>
                        <Row>
                            <Col>
                                <SearchBar />
                            </Col>
                        </Row>
                    </Container>
                </>
            }>
            </Route>

            <Route path="/web3" element={
                <>
                    <Loader />
                    <Container>
                        <Web3 />
                    </Container>
                </>
            }>
            </Route>

            <Route path="/rickandmorty" element={
                <>
                    <Loader />
                    <Container>
                        <RickAndMorty />
                    </Container>
                </>
            }>
            </Route>

            <Route path="/article" element={
                <>
                    <Container>
                        <Article />
                    </Container>
                </>
            }>
            </Route>

        </Routes>
    );
}
