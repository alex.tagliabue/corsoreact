import { combineReducers } from 'redux';
import { userReducer } from '../../../user/store';
import { coinsReducer } from '../../../web3';
import { characterReducer } from '../../../rickandmorty'
import { articleReducer } from '../../../article'

export const rootReducer = combineReducers({ userState: userReducer, coinState: coinsReducer, characterState: characterReducer, articleState: articleReducer });
