import { all } from 'redux-saga/effects'
import { userSaga } from '../../../user'
import { coinsSaga } from '../../../web3'
import { characterSaga } from '../../../rickandmorty'
import { articleSaga } from '../../../article'

export function* rootSaga() {
    yield all([userSaga(), coinsSaga(), characterSaga(), articleSaga()])
}