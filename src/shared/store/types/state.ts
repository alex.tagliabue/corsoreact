import { userState } from "../../../user/types"

export type NotesState = {
    notes: string[];
    userState: userState;
}