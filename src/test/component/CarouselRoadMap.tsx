import { Carousel, Col, Row } from "react-bootstrap"
import './CarouselRoadMap.css'

export const CarouselRoadMap = () => {
    return (
        <Carousel>

            <Carousel.Item interval={3000}>
                <div>
                    <Row>
                        <Col xs={12} sm={6} className="p-5">
                            <img
                                className="d-block w-100 img-fluid"
                                src="https://m.media-amazon.com/images/I/71kJp2AHgyL._AC_SX355_.jpg"
                                alt="First slide"
                            />
                        </Col>
                        <Col xs={12} sm={6} className="p-5 d-flex align-items-center justify-content-center">
                            <div>
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid officia impedit veritatis iusto? Fugiat unde ea minima! Qui, hic doloremque.</p>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Carousel.Item>

            <Carousel.Item interval={3000}>
                <div>

                    <Row>
                        <Col xs={12} sm={6} className="p-5">
                            <img
                                className="d-block w-100 img-fluid"
                                src="https://m.media-amazon.com/images/I/71kJp2AHgyL._AC_SX355_.jpg"
                                alt="First slide"
                            />
                        </Col>
                        <Col xs={12} sm={6} className="p-5 d-flex align-items-center justify-content-center">
                            <div className="d-flex align-item-center justify-content-center">
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid officia impedit veritatis iusto? Fugiat unde ea minima! Qui, hic doloremque.</p>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Carousel.Item>

            <Carousel.Item interval={3000}>
                <div>
                    <Row>
                        <Col xs={12} sm={6} className="p-5">
                            <img
                                className="d-block w-100 img-fluid"
                                src="https://m.media-amazon.com/images/I/71kJp2AHgyL._AC_SX355_.jpg"
                                alt="Second slide"
                            />
                        </Col>
                        <Col xs={12} sm={6} className="p-5 d-flex align-items-center justify-content-center">
                            <div className="d-flex align-item-center justify-content-center">
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid officia impedit veritatis iusto? Fugiat unde ea minima! Qui, hic doloremque.</p>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Carousel.Item>

            <Carousel.Item interval={3000}>
                <div>
                    <Row>
                        <Col xs={12} sm={6} className="p-5">
                            <img
                                className="d-block w-100 img-fluid"
                                src="https://m.media-amazon.com/images/I/71kJp2AHgyL._AC_SX355_.jpg"
                                alt="Third slide"
                            />
                        </Col>
                        <Col xs={12} sm={6} className="p-5 d-flex align-items-center justify-content-center">
                            <div className="d-flex align-item-center justify-content-center">
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid officia impedit veritatis iusto? Fugiat unde ea minima! Qui, hic doloremque.</p>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Carousel.Item>

            <Carousel.Item interval={3000}>
                <div>
                    <Row>
                        <Col xs={12} sm={6} className="p-5">
                            <img
                                className="d-block w-100 img-fluid"
                                src="https://m.media-amazon.com/images/I/71kJp2AHgyL._AC_SX355_.jpg"
                                alt="Fourth slide"
                            />
                        </Col>
                        <Col xs={12} sm={6} className="p-5 d-flex align-items-center justify-content-center">
                            <div>
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid officia impedit veritatis iusto? Fugiat unde ea minima! Qui, hic doloremque.</p>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Carousel.Item>

        </Carousel>

    )
} 