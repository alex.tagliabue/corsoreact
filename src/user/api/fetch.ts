import { API_URL } from "../../shared/const";
import { User, userListResponse } from "../types";
import axios from 'axios';

export const callCreateUser = async (u: User): Promise<User> => {

    console.log('async start');

    const callResult = await axios.post(`${API_URL}users`, u)
        .then((r) => {
            console.log('anadata a fuon fine la post');
            if (r.status === 201) {
                return r.data;
            }
            else {
                return null;
            }
        })
        .catch((e) => {
            throw e;
        })

    return callResult;

}


export const callUsersList = async (page: number): Promise<userListResponse> => {
    console.log('async start');

    const callResult = await axios.get(`${API_URL}users?page=${page}`)
        .then((r) => {
            if (r.status === 200) {
                return r.data;
            }
            else {
                return null;
            }
        })
        .catch((e) => {
            throw e;
        })

    return callResult;
}
