
import { Card } from "react-bootstrap";
import { userList } from "../../types";

export const ListUserCard= (p:{user:userList})=>{
    return (
        <Card style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Title>{p.user.first_name}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">{p.user.last_name}</Card.Subtitle>
                <Card.Subtitle className="mb-2 text-muted">{p.user.email}</Card.Subtitle>
                <Card.Subtitle className="mb-2 text-muted">{p.user.id}</Card.Subtitle>
                <Card.Img variant="top" src={p.user.avatar} ></Card.Img>
            </Card.Body>
        </Card>
    );
}