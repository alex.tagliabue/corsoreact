import { UserCardProps } from '../../types'
import { Card } from 'react-bootstrap'

export const UserCard = (p: UserCardProps) => {
    const { user } = p;
    return (
        <Card style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Title>{user.name}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">{user.job}</Card.Subtitle>
                <Card.Subtitle className="mb-2 text-muted">{user.id}</Card.Subtitle>
                <Card.Subtitle className="mb-2 text-muted">{user.createdAt}</Card.Subtitle>
            </Card.Body>
        </Card>
    );
}