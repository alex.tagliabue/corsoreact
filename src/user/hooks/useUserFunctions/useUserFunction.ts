import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import { startSagaLoaduserAction} from '../../store/actions'
import { startSagaPayload } from '../../types';

export const useUserFunctions = () => {

    const dispatch = useDispatch();

    const startSagaLoaduser = useCallback((r:startSagaPayload)=>{
        dispatch(startSagaLoaduserAction(r))
    },[dispatch])

    return { startSagaLoaduser }
}