import { createAction } from "typesafe-actions";
import { userList, startSagaPayload } from "../..";

const startSagaLoaduser = "users/startSalaLoad";
const callLoadUsers = "users/callLoad";
const userLoaded = "users/loaded";


export const startSagaLoaduserAction = createAction(startSagaLoaduser)<startSagaPayload>();

export const callLodedUsersAction = createAction(callLoadUsers)();

export const usersLoadedAction = createAction(userLoaded)<userList[]>();