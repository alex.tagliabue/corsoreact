import * as actions from '../actions';
import { userState } from '../../types';

import { ActionType, createReducer } from 'typesafe-actions'

const initialState: userState = {
    loading: false,
    users: [],
}


export type UserActionList = ActionType<typeof actions>

export type callLodedUsersActionType = ActionType<typeof actions.callLodedUsersAction>

export type usersLoadedActionType = ActionType<typeof actions.usersLoadedAction>


const callLodedUsersActionTypeCase = (state: userState, action: callLodedUsersActionType): userState => ({
    ...state, loading: true

});

const usersLoadedActionTypeCase = (state: userState, action: usersLoadedActionType): userState => ({
    ...state, users: [...action.payload], loading: false
});

export const userReducer = createReducer<userState, UserActionList>(initialState).
    handleAction(
        actions.callLodedUsersAction,
        callLodedUsersActionTypeCase
    ).handleAction(
        actions.usersLoadedAction,
        usersLoadedActionTypeCase
    )