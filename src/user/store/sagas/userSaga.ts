import * as actions from '../actions'
import { call, put, takeLatest } from 'redux-saga/effects'
import { callUsersList } from '../../api'
import { userListResponse } from '../..'

function* loadUsersSaga(action: ReturnType<typeof actions.startSagaLoaduserAction>) {

    yield put(actions.callLodedUsersAction())

    try {
        let usersResponse: userListResponse = yield call(callUsersList, action.payload.page)
        yield put(actions.usersLoadedAction(usersResponse.data));

    } catch (error) {

    }
}

export function* userSaga() {
    yield takeLatest(actions.startSagaLoaduserAction, loadUsersSaga)
}