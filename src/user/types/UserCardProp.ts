import { User } from "./User";

export type UserCardProps = {
    user: User
}