import CoinGecko from 'coingecko-api'
import {CoinsAllParams, SimplePriceParams, CoinGecoResponse, CoinListResponse,PriceResponse} from '../types';

const CoinGeckoClient = new CoinGecko();

export const getCoinList = async (params?: CoinsAllParams):Promise<CoinListResponse> => {
    let data = await CoinGeckoClient.coins.all(params)
    console.log('Coin list', data);
    //console.log('JSON', JSON.stringify(data.data[0]));
    return data;
}

export const getCoinsData = async (params: SimplePriceParams):Promise<PriceResponse> => {
    let data = await CoinGeckoClient.simple.price(params)
    console.log('Coins data', data);
    return data;
}