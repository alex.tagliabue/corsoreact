import { useStoreCoins } from '../../store';
import { useMemo } from 'react';
import { Row, Col } from 'react-bootstrap';

export const CoinListComponent = () => {

    const { storeCoins } = useStoreCoins();

    const showListaCoin = useMemo(() => {
        const lista = storeCoins.map((c) => {
            return (
            <Row key={c.id}>
                <Col><img src={c?.image?.thumb} alt="" /></Col>
                <Col><b>{c.name}</b></Col>
                <Col><b>{c.symbol}</b></Col>
                <Col><b>{c.market_data.price_change_24h}</b></Col>
            </Row>
            )})
        return lista;

    }, [storeCoins])

    return (
        <>
            {showListaCoin}
        </>
    );
}