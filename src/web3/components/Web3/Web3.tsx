import { useEffect, useMemo, useState } from "react";
import { Row, Col, Button } from "react-bootstrap";
import { tokenAddress } from "../..";
import { CoinListComponent } from "../CoinListComponent";
import web3 from "web3"

import {useCoinFunction} from '../../hooks' 

import tokenABI from "./tokenABI";
import { setInterval } from "timers";

export const Web3 = () => {

    const [currentAccount, setCurrentAccount] = useState();
    const [linkBalance, setLionkBalance] = useState(0);
    const { ethereum } = window as any
    const { startSagaLoadCoins } = useCoinFunction();

    const web3instance = new web3(ethereum);

    const linkaddress: tokenAddress = {
        address: '0x01be23585060835e02b77ef475b0cc51aa1e0709',
        token: 'LINK'
    }

    const checkAccounts = async () => {

        //const { ethereum } = window as any

        if (!ethereum) {
            alert("installare metamask e rilanciare l'Applicazione")
            return;
        } else {
            const accounts = await ethereum.request({ method: 'eth_accounts' });

            if (accounts.length !== 0) {
                const account = accounts[0];
                console.log("Found an authorized account: ", account);
                setCurrentAccount(account);
            } else {
                console.log("No authorized account found");
            }
        }
    }

    const connectWalletHandler = async () => {
        //const { ethereum } = window as any;

        if (!ethereum) {
            alert("Please install Metamask!");
        }

        try {
            const accounts = await ethereum.request({ method: 'eth_requestAccounts' });
            console.log("Found an account! Address: ", accounts[0]);
            setCurrentAccount(accounts[0]);
        } catch (err) {
            console.log(err)
        }
    }

    const getBalance = async () => {
        const tokenInst = new web3instance.eth.Contract(tokenABI as any, linkaddress.address);
        const balance = await tokenInst.methods.balanceOf(currentAccount).call();
        setLionkBalance(balance);
    } 

    useEffect(() => {
        checkAccounts();
        startSagaLoadCoins(undefined)

        /*
        setInterval(()=>{
            startSagaLoadCoins(undefined)
        },5000)
        */
       
    }, []);

    useEffect(()=>{
        if(currentAccount)
        getBalance();
    }, [currentAccount]);

    const showConnectAccount = useMemo(() => {
        if (currentAccount) {
            return <span>Connected account: {currentAccount}</span>
        } else {
            return <Button onClick={() => connectWalletHandler()}>Connect Metamask</Button>
        }
    }, [currentAccount])

    return (
        <>

            <Row>
                <Col xs={12}>
                    <h1>Web 3</h1>
                    {showConnectAccount}
                </Col>
                <Col xs={12}>
                    Link token balance: {linkBalance}
                </Col>
                <Col xs={12}>
                    <Button onClick={() => {startSagaLoadCoins(undefined)}}>Coin List</Button>
                </Col>
            </Row>
            <CoinListComponent/>
        </>
    );
}