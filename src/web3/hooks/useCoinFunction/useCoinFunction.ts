import { useDispatch } from 'react-redux';
import { useCallback } from 'react';

import { startSagaCoinListAction} from '../../store/actions'
import { CoinsAllParams } from '../../types';

export const useCoinFunction = () => {

    const dispatch = useDispatch();

    const startSagaLoadCoins = useCallback((r:CoinsAllParams | undefined)=>{
        dispatch(startSagaCoinListAction(r))
    },[dispatch])

    return { startSagaLoadCoins }
}