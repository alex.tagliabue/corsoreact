import { createAction } from "typesafe-actions";
import { CoinListDetiail, CoinsAllParams } from "../..";

const startSagaCoinList = "web3/startSagaList";

const callLoadCoins = "web3/callLoad";

const coinsLoaded = "web3/loaded";



export const startSagaCoinListAction = createAction(startSagaCoinList)<CoinsAllParams | undefined>();

export const callLoadCoinsAction = createAction(callLoadCoins)();

export const coinsLoadedAction = createAction(coinsLoaded)<CoinListDetiail[]>();

