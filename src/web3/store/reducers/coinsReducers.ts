import * as actions from '../actions';
import { coinState } from '../../types';

import { ActionType, createReducer } from 'typesafe-actions'

const initialState: coinState = {
    loading: false,
    coins: [],
}

export type CoinsActionList = ActionType<typeof actions>

export type startSagaCoinListActionType = ActionType<typeof actions.startSagaCoinListAction>

export type callLoadCoinsActionType = ActionType<typeof actions.callLoadCoinsAction>

export type coinsLoadedActionType = ActionType<typeof actions.coinsLoadedAction>


const callLoadCoinsActionTypeCase = (state: coinState, action: callLoadCoinsActionType): coinState => ({
    ...state, loading: true
});

const coinsLoadedActionTypeCase = (state: coinState, action: coinsLoadedActionType): coinState => ({
    ...state, coins: [...action.payload], loading: false
});

export const coinsReducer = createReducer<coinState, CoinsActionList>(initialState).
    handleAction(
        actions.callLoadCoinsAction,
        callLoadCoinsActionTypeCase
    ).handleAction(
        actions.coinsLoadedAction,
        coinsLoadedActionTypeCase
    )