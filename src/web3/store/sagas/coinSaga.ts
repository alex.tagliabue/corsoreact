import * as actions from '../actions'
import { call, put, takeLatest } from 'redux-saga/effects'

import { getCoinList } from '../../api'
import { CoinListResponse } from '../..'

function* loadCoinsSaga(action: ReturnType<typeof actions.startSagaCoinListAction>) {

    yield put(actions.callLoadCoinsAction())

    try {
        let coinResponse: CoinListResponse = yield call(getCoinList, action.payload)
        yield put(actions.coinsLoadedAction(coinResponse.data));

    } catch (error) {

    }
}

export function* coinsSaga() {
    yield takeLatest(actions.startSagaCoinListAction, loadCoinsSaga)
}