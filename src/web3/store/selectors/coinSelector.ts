import { createSelector } from 'reselect';
import { useSelector } from 'react-redux';
import { coinState } from '../..';


const sliceState = (state: { coinState: coinState }) => state.coinState;

const storeCoinSelector = createSelector(sliceState, slice => slice.coins)

const loadingSelector = createSelector(sliceState, slice => slice.loading)

export const useStoreCoins = () => {
    const storeCoins = useSelector(storeCoinSelector);
    return { storeCoins }
}

export const useCoinsAreLoading = () => {
    const loading = useSelector(loadingSelector);
    return { loading }
}